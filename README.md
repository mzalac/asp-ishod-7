# ASP - Ishod 7

Rezultati A priori mjerenja za Segment 2(dvostruko vezana lista) i Segment 3(jednostruko vezana lista)


| operacija | dvostruko vezana lista | jednostruko vezana lista |
|---------- |---------------------- | ------------------------ |
| ubacivanje 10 000 elemenata na pocetak|51455156 ns = 0,051 s  | 2267991 ns = 0,002 s     | 
| ubacivanje 10 000 elemenata na sredinu| 1990138740 ns = 1,99 s | 121982657 ns = 0,0122 s  | 
| ubacivanje 10 000 elemenata na kraj | 1877132 ns = 0,0018 s | 1590462 ns = 0,0015 s    |
| dohvaćanje elementa na indexu 1000 | 432178 ns = 0,000432 s| 448872 ns = 0,00044 s   |
| brisanje 1000 elemenata s pocetka | 1777026 ns = 0,00178 s | 76818 ns = 7,6818x10-5 s |
| brisanje 1000 elemenata sa sredine | 118533 ns = 0,00012 s  | 58331 ns = 5,8331x10-5 s |
| brisanje 1000 elemenata s kraja | 224419 ns = 0,000224 s | 86773 ns = 8,6773x10-5 s |


Kao što možemo vidjeti iz priloženih rezultata, ubacivanje elemenata u jednostruko vezanu listu odvija se brže nego ista operacija u dvostruko vezanoj listi. To ima smisla jer dvostruko vezana lista sadrži i pokazivače na prijašnje elemente, kao i sljedeće, dok jednostruko vezana lista sadrži samo pokazivače/podatak o sljedećem elemetnu. Drugim riječima, neki element u jednostruko vezanoj listi ne zna koji se element nalazi prije njega, več samo poslije. Usporedivši to s dvostruko vezanom listom, ubacivanje elemenata iči će brže jer se program ne opterećuje umetanjem dodatnih podataka. 

Dohvaćanje nekog elementa nama poznate vrijednosti odvijat će se brže u dvosturko vezanoj listi iz razloga što je jednostavnije proći kroz sve elemente takve liste, nego jednostruko vezane liste, zbog "bolje" međusobne povezanosti elemenata unutar takve liste.

Brisanje elemenata bilo to s početka, kraja ili sredine uvijek će se brže odvijati u jednostruko vezanoj listi nego dvostruko vezanoj listi jer imamo manje podataka za izbrisati. Ponovno se vraćamo na međusobnu povezanost elemenata u ovim listama. Uzmimo primjer brisanja jednog elementa iz obje liste. Kod jednostruko vezane liste briše se sam podatak, nakon čega nestaje i njegov pokazivač na sljedeći element. Tad element prije izbrisanog svoj pokazivač šalje na sljedeći (onaj poslije izbrisanog). Kod dvostruko vezane liste, postupak je dosta sličan ali element nakon obrisanog mora voditi evidenciju da je onaj prije njega obrisan i nedostaje mu "stražnji" pokazivač. To naravno zahtjeva više resursa računala i samim time je postupak dugotrajniji što u ovom slučaju čini dvostruko vezanu listu sporijom.
